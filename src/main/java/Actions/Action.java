
package Actions;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Origin.base.Base;

public class Action extends Base {

  static String scrShotDir = "screenshots";
  File scrFile;
  static File scrShotDirPath = new java.io.File("./" + scrShotDir + "//");
  String destFile;

  public void navigate(String url) {
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    driver.navigate().to(url);
  }

  public void click(WebElement element) throws InterruptedException {
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    WebDriverWait wait = new WebDriverWait(driver, 30);
    wait.until(ExpectedConditions.elementToBeClickable(element));
    element.click();
  }


  public WebElement tableBody(WebElement element, String row, int column) throws InterruptedException {
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

   List<WebElement>rows = element.findElements(By.xpath("./tbody/tr"));
 
    for (WebElement x : rows) {
      if (x.getText().contains(row)) {
        List<WebElement>columns=  x.findElements(By.xpath("./td"));
        WebElement y =columns.get(column);
      return y;
      }
    }
    return null;
  

  }
  public WebElement tableHeader(WebElement element, int column) throws InterruptedException {
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

   WebElement header = element.findElement(By.xpath("./thead/tr"));
 
        List<WebElement>columns=  header.findElements(By.xpath("./th"));
        WebElement y =columns.get(column);
      return y;
      }

}
