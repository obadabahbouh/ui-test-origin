package Origin.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Base {
	protected static WebDriver driver;;
	protected FileInputStream inputStream;
	protected Properties prop;
	public static ExtentReports extent;
	public static ExtentTest logger;
  
	@BeforeSuite
	public void beforeSuite() {
		extent = new ExtentReports("Reports/index.html");
		extent.addSystemInfo("Author", "Obada");
		extent.addSystemInfo("App", "Origin-web");

	}

	@AfterSuite
	public void afterSuite() {
		extent.flush();
	}

	@BeforeMethod
	public void beforeMethod(Method method) {
		// Create a new ChromeDriver
		
		ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-notifications");
        options.setPageLoadStrategy(PageLoadStrategy.NONE);
		System.setProperty("webdriver.chrome.driver", "/Users/alpha/Downloads/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
driver.manage().deleteAllCookies();
driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//logger = extent.startTest(method.getName());
	}

	@AfterMethod
	public void afterMethod(Method method, ITestResult result) {


		driver.quit();
	}

}
