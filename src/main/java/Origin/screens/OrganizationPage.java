package Origin.screens;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Origin.base.Base;


public class OrganizationPage extends Base{

	public String loginURL="https://cms.dev.originall.com/";
 
	@FindBy(xpath = "//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[1]/header/div/div[1]/ul/li[2]/nav/ol/li/h6/span")
	@CacheLookup
	public WebElement pageTitle;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[1]/div/div[1]/p/span")
	@CacheLookup
	public WebElement tableTitle;

	@FindBy(id  ="add")
	@CacheLookup
	public WebElement addBtn;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[1]/div/div[2]/button[2]")
	@CacheLookup
	public WebElement clearFilter;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[1]/div/div/div/div/div[1]/div[2]/nav/ul/div/div/div/ul")
	@CacheLookup
	public WebElement addOrgTitle;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div[2]/div/form/div[1]/div/div/div/input")
	@CacheLookup
	public WebElement addName;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div[2]/div/form/div[2]/div/div/div/textarea[1]")
	@CacheLookup
	public WebElement addDescroption;

	@FindBy(xpath ="/html/body/div[1]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div[2]/div/form/div[3]/div/div/div/input")
	@CacheLookup
	public WebElement addCountry;

	@FindBy(xpath = "//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div[2]/div/form/div[4]/span/button")
	@CacheLookup
	public WebElement addIndusrtyBtn;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div[2]/div/form/div[1]/div/div/div/input")
	@CacheLookup
	public WebElement industryName;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div[2]/div/form/div[2]/div/div/div/textarea[1]")
	@CacheLookup
	public WebElement industryDesc;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div[2]/div/form/button")
	@CacheLookup
	public WebElement createIndusrtyBtn;
	
	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div[2]/div/form/div[5]/span/button")
	@CacheLookup
	public WebElement addProductMetaBtn;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div[2]/div/form/div[1]/div/div/div/input")
	@CacheLookup
	public WebElement ProductMetaName;

	@FindBy(xpath ="/html/body/div[1]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div[2]/div/form/div[2]/div/div/div/div")
	@CacheLookup
	public WebElement ProductMetaStatus;

	@FindBy(xpath ="/html/body/div[4]/div[3]/ul")
	@CacheLookup
	public WebElement ProductMetaStatusNav;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div[2]/div/form/button")
	@CacheLookup	
	public WebElement createProductMetaBtn;
	

	public OrganizationPage(){
		PageFactory.initElements(driver, this);
	}



}
