package Origin.screens;
import java.rmi.server.ExportException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Origin.base.Base;


public class LoginPage extends Base{

	public String loginURL="https://cms.dev.originall.com/";
 
	@FindBy(name ="email")
	@CacheLookup
	public WebElement email;

	@FindBy(name ="password")
	@CacheLookup
	public WebElement password;

	@FindBy(xpath ="//*[@id=\"root\"]/section/div/div[2]/div/form/div[4]/button")
	@CacheLookup
	public WebElement loginBtn;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[1]/div/div/div/div/div[1]/div[2]/nav/ul")
	@CacheLookup
	public WebElement navBar;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[1]/div/div/div/div/div[1]/div[2]/nav/ul/div/div/div/ul")
	@CacheLookup
	public WebElement navBarReports;

	@FindBy(xpath ="//*[@id=\"menu-status\"]/div[3]/ul")
	@CacheLookup
	public WebElement navBarStatus;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[2]/div/div/div[1]/table")
	@CacheLookup
	public WebElement userTable;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[2]/div/div/div[1]/table")
	@CacheLookup
	public WebElement userStatsTable;

	@FindBy(id ="mui-component-select-status")
	@CacheLookup
	public WebElement statusMapStats;

	@FindBy(name ="name")
	@CacheLookup
	public WebElement userName;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div[2]/div/form/button")
	@CacheLookup
	public WebElement editUserButton;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[1]/header/div/div[2]/div/div/button")
	@CacheLookup
	public WebElement userIcon;
	
	@FindBy(xpath ="//*[@id=\"simple-menu\"]/div[3]/ul/li[2]")
	@CacheLookup
	public WebElement logout;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div/div/div/div[2]/div/div/div[2]/nav/ul")

	@CacheLookup
	public WebElement pagenatorUserState;

	@FindBy(xpath ="//*[@id=\"root\"]/div[2]/div/div/div[3]/div/div/div[2]/div/div[1]/div/div/div/div[3]/div/div/div/div[2]/div/div/div/div[2]/div/div")
	@CacheLookup
	public WebElement pagenatorUserDetails;

	@FindBy(xpath ="//*[@id=\"menu-\"]/div[3]/ul")
	@CacheLookup
	public WebElement pagenatorNav;

	public LoginPage(){
		PageFactory.initElements(driver, this);
	}




}
