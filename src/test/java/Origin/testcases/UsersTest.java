package Origin.testcases;

import java.util.List;

import com.beust.jcommander.internal.Console;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import Actions.Action;

import Origin.screens.*;


public class UsersTest extends Action {


	LoginPage loginPage;

	public static String url="https://cms.dev.originall.com/";

	@Test(priority = 1, enabled = true)
	public void editUserNameSuccessfully() throws InterruptedException {

		loginPage = new LoginPage();
		navigate(loginPage.loginURL);	
		loginPage.email.sendKeys("admin@admin.com");
		loginPage.password.sendKeys("12345678");
		click(loginPage.loginBtn);
		Thread.sleep(2000);
		final String text1 = driver.getCurrentUrl();
		System.out.println(text1);
		Thread.sleep(2000);
		Assert.assertTrue(text1.contains("app/organizations"));
		Thread.sleep(3000);
	    click(loginPage.navBar.findElements(By.tagName("li")).get(6));
		Thread.sleep(2000);
		click(loginPage.pagenatorUserState);
		Thread.sleep(4000);
	
		tableBody(loginPage.userStatsTable,"ZainStatistics",0).findElement(By.xpath("./div/a")).click();
		Thread.sleep(3000);
		click(loginPage.pagenatorUserDetails);
		Thread.sleep(2000);
		click(loginPage.pagenatorNav.findElements(By.tagName("li")).get(1));
		Thread.sleep(4000);
		loginPage = new LoginPage();
		click(loginPage.navBar.findElements(By.tagName("li")).get(8));
		Thread.sleep(2000);
		click(loginPage.navBarReports.findElements(By.tagName("li")).get(1));
		Thread.sleep(6000);
		click(loginPage.statusMapStats);
		Thread.sleep(2000);
		click(loginPage.navBarStatus.findElements(By.tagName("li")).get(0));
		Thread.sleep(3000);
		
		loginPage = new LoginPage();
		click(loginPage.statusMapStats);
		click(loginPage.navBarStatus.findElements(By.tagName("li")).get(1));
		Thread.sleep(3000);
	
		loginPage = new LoginPage();
		click(loginPage.statusMapStats);
		click(loginPage.navBarStatus.findElements(By.tagName("li")).get(2));
		Thread.sleep(7000);
	

	}
	






}
