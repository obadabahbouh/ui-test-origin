package Origin.testcases;

import java.util.List;

import com.beust.jcommander.internal.Console;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Actions.Action;

import Origin.screens.*;


public class LoginScreenTest extends Action {


	LoginPage loginPage;
	public static String url="https://cms.dev.originall.com/";

	@Test(priority = 1, enabled = false)
	public void LoginSuccessfully() throws InterruptedException {
		loginPage = new LoginPage();
		navigate(loginPage.loginURL);
		loginPage.email.sendKeys("admin@admin.com");
		Thread.sleep(1000);
		loginPage.password.sendKeys("12345678");
		click(loginPage.loginBtn);
		Thread.sleep(2000);
		final String text1 = driver.getCurrentUrl();
		Thread.sleep(2000);
		System.out.println(text1);
		Assert.assertTrue(text1.contains("app/organizations"));

	}
	@Test(priority = 2, enabled = true)
	public void editUserNameSuccessfully() throws InterruptedException {
		loginPage = new LoginPage();
		navigate(loginPage.loginURL);
		loginPage.email.sendKeys("admin@admin.com");
		loginPage.password.sendKeys("12345678");
		click(loginPage.loginBtn);
		Thread.sleep(2000);
		final String text1 = driver.getCurrentUrl();
		System.out.println(text1);
		Assert.assertTrue(text1.contains("app/organizations"));
	    click(loginPage.navBar.findElements(By.tagName("li")).get(1));
		tableBody(loginPage.userTable,"normal",6).findElement(By.xpath("./span[1]/button")).click();
		Thread.sleep(1000);
		String s = Keys.chord(Keys.COMMAND, "a");
		loginPage.userName.sendKeys(s);
		loginPage.userName.sendKeys(Keys.BACK_SPACE);
		Thread.sleep(1000);
		loginPage.userName.sendKeys("test user");
		Thread.sleep(3000);
		click(loginPage.editUserButton);
		Thread.sleep(3000);
		loginPage = new LoginPage();
		tableBody(loginPage.userTable,"normal",1).click();

	}
	@Test(priority = 3, enabled = false)
	public void LogoutSuccessfully() throws InterruptedException {
		loginPage = new LoginPage();
		navigate(loginPage.loginURL);
		loginPage.email.sendKeys("admin@admin.com");
		loginPage.password.sendKeys("12345678");
		click(loginPage.loginBtn);
		Thread.sleep(2000);
		final String text1 = driver.getCurrentUrl();
		System.out.println(text1);
		Assert.assertTrue(text1.contains("app/organizations"));
		Thread.sleep(3000);
		click(loginPage.userIcon);
		click(loginPage.logout);
		Thread.sleep(2000);
		final String text2 = driver.getCurrentUrl();
		System.out.println(text2);
		Assert.assertTrue(text2.contains("/signin"));
		

		

	}






}
