package Origin.testcases;

import java.util.List;

import com.beust.jcommander.internal.Console;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import Actions.Action;

import Origin.screens.*;


public class OrganizationsTest extends Action {


	LoginPage loginPage;
	OrganizationPage organizationPage;

	public static String url="https://cms.dev.originall.com/";

	@Test(priority = 1, enabled = true)
	public void checkOrgPageTitles() throws InterruptedException {

		loginPage = new LoginPage();
		organizationPage=new OrganizationPage();
		navigate(loginPage.loginURL);	
		loginPage.email.sendKeys("admin@admin.com");
		loginPage.password.sendKeys("12345678");
		click(loginPage.loginBtn);
		Thread.sleep(2000);
		final String text1 = organizationPage.pageTitle.getText();
		Thread.sleep(2000);
		Assert.assertTrue(text1.contains("Organizations"));
		Thread.sleep(2000);
		final String text2 = organizationPage.tableTitle.getText();
		Thread.sleep(2000);
		Assert.assertTrue(text2.contains("Organizations Table"));
	  
	}

	
	






}
